# Usage:

Step 1: Start ROS master 
    
    roscore

Step 2: Set up/Export path to qpid directory.e.g.
  
    export Avatar_HOME=/home/user_name/qpid/

Step 3: Start qpid server 

    qpid-server
      Note: requires JAVA_HOME and PATH enviornment variables to be pointing to Java 1.6

### Running with simulation:

Step 4: Set up/Export path to avatar installation directory.e.g.
  
    export Avatar_HOME=/home/user_name/zeno_avatar_simulator/

Step 5: Start zeno avatar simulator
    
    roslaunch zeno_avatar_simulation avatar_simulation.launch
      Note: requires JAVA_HOME and PATH enviornment variables to be pointing to Java 1.6

Step 6: Set up/Export environment variable 'Avatar_Simulation' to true e.g.
  
    export Avatar_Simulation=true

### Running with real robot:

Step 8: Login to zeno using SSH and start required services.
    sudo confctl services start:headless
    sudo confctl services start:qpid

    Note: After running the services, make sure the robot joints are activated.

Step 7: Set up/Export environment variable 'Avatar_Simulation' to false e.g.
 
    export Avatar_Simulation=false


## Launch interface to zeno robokind api

Step 8:

    roslaunch zeno_robokind_interface robokind_interface.launch
      Note: requires JAVA_HOME and PATH enviornment variables to be pointing to Java 1.7
