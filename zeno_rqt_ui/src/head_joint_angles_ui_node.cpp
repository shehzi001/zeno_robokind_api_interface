#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>

#include <zeno_rqt_ui/zenoHeadJointsConfig.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <vector>
#include <string>
#define number_of_joints 11

ros::Publisher pub_;

std::string names[number_of_joints] = {"neck_yaw", "neck_roll", "neck_pitch", 
										"jaw","brows","eyelids","eyes_pitch",
										"right_eye_yaw", "left_eye_yaw","left_smile", "right_smile"
                 					  };


void dynamicReconfigureBodyDegreesCallback(zeno_rqt_ui::zenoHeadJointsConfig &config, uint32_t level);

void dynamicReconfigureBodyDegreesCallback(zeno_rqt_ui::zenoHeadJointsConfig &config, uint32_t level)
{
	trajectory_msgs::JointTrajectory trajectory;
	trajectory_msgs::JointTrajectoryPoint trajectory_points;
	trajectory.points.resize(1);
	std::vector<std::string> joint_names;
	
	int msg_name_index = 0;
	int name_counter=0;
	int resize_counter = 0;
	if(config.Enable_head_joints_cmd)
	{
		if (config.neck) {
			resize_counter += 3;
			joint_names.resize(resize_counter);
			trajectory_points.positions.resize(resize_counter);

			for(int index = msg_name_index; index < msg_name_index+3; index++) {
				joint_names.at(index) = names[name_counter];
				name_counter++;
			}
			trajectory_points.positions[msg_name_index] = config.neck_yaw;
			trajectory_points.positions[msg_name_index+1] = config.neck_roll;
			trajectory_points.positions[msg_name_index+2] = config.neck_pitch;

			msg_name_index += 3;

		}else {
			name_counter += 3;
		}

		if (config.mouth) {
			resize_counter++;
			joint_names.resize(resize_counter);
			trajectory_points.positions.resize(resize_counter);

			joint_names.at(msg_name_index) = names[name_counter];
			trajectory_points.positions[msg_name_index] = config.mouth_jaw;
			msg_name_index++;
			name_counter++;
		}
		else {
			name_counter++;
		}

		if (config.eyes) {
			
			resize_counter += 5;
			joint_names.resize(resize_counter);
			trajectory_points.positions.resize(resize_counter);

			for(int index = msg_name_index; index < msg_name_index+5; index++) {
				joint_names.at(index) = names[name_counter];
				name_counter++;
			}
			//std::cout << "left_arm" << std::endl;
			trajectory_points.positions[msg_name_index] = config.eye_brows_pitch;
			trajectory_points.positions[msg_name_index+1] = config.eye_lids;
			trajectory_points.positions[msg_name_index+2] = config.eyes_pitch;
			trajectory_points.positions[msg_name_index+3] = config.right_eye;
			trajectory_points.positions[msg_name_index+4] = config.left_eye;

			msg_name_index += 5;

		}else {
			name_counter += 5;
		}

		if (config.smiles) {
			resize_counter += 2;
			joint_names.resize(resize_counter);
			trajectory_points.positions.resize(resize_counter);

			for(int index = msg_name_index; index < msg_name_index+2; index++) {
				joint_names.at(index) = names[name_counter];
				name_counter++;
			}

			trajectory_points.positions[msg_name_index] = config.left_smile;
			trajectory_points.positions[msg_name_index+1] = config.right_smile;

			msg_name_index += 2;


		}else {
			name_counter += 2;
		}


		trajectory.joint_names = joint_names;

		trajectory.points[0] = trajectory_points;
		if(sizeof(joint_names) != 0)
			pub_.publish(trajectory);
	}
}

int main (int argc, char** argv)
{
	// Initialize ROS
	ros::init (argc, argv, "head_joint_angles_ui_node");
	ros::NodeHandle nh("~");

	pub_ = nh.advertise<trajectory_msgs::JointTrajectory>("/head_controller/trajectory_command_sim", 100);

	dynamic_reconfigure::Server<zeno_rqt_ui::zenoHeadJointsConfig> server_degrees;
	dynamic_reconfigure::Server<zeno_rqt_ui::zenoHeadJointsConfig>::CallbackType f_degrees;
	f_degrees = boost::bind(&dynamicReconfigureBodyDegreesCallback, _1, _2);
	server_degrees.setCallback(f_degrees);

	ROS_INFO("Head Joints rqt GUI node is running. Use rqt_reconfigure to test joints.");
	ros::spin();
}
