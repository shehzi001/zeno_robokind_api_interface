cmake_minimum_required(VERSION 2.8.3)
project(zeno_rqt_ui)

find_package(catkin REQUIRED COMPONENTS
  dynamic_reconfigure
  roscpp
  rospy
  trajectory_msgs
  std_msgs
)

generate_dynamic_reconfigure_options(
  cfg/head_joint_angles.cfg
  cfg/body_joint_angles.cfg
)

catkin_package(
  CATKIN_DEPENDS dynamic_reconfigure roscpp rospy
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

#==========================================
add_executable(head_joint_angles_ui_node 
  src/head_joint_angles_ui_node.cpp)

add_dependencies(head_joint_angles_ui_node 
  ${PROJECT_NAME}_gencfg)

target_link_libraries(head_joint_angles_ui_node
  ${catkin_LIBRARIES}
)

#==========================================
add_executable(body_joint_angles_ui_node 
  src/body_joint_angles_ui_node.cpp)

add_dependencies(body_joint_angles_ui_node 
  ${PROJECT_NAME}_gencfg)

target_link_libraries(body_joint_angles_ui_node
  ${catkin_LIBRARIES}
)

