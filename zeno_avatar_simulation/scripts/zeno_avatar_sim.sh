#!/bin/sh

load_java6_config &

qpid-server &

sleep 2

if [ -n $Avatar_HOME ]; then
   cd $Avatar_HOME && mvn -Prun-on-felix package antrun:run
else
   echo "Environment variable Avatar_HOME is not configured."
fi
