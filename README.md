# Important usage Instruction
The softwares installation needs to be done on remote workstation. It is not required to be on zeno's computer except some commands/instructions(mentioned in this wiki) which are required to initialize communication between zeno's pc and remote workstation.

# Pre-requisite installations:
  * Robot Operating System (ROS).
  * rosjava
  * maven2
  * qpid (download qpid-broker-0.16 and extract to e.g. /home/user_name/qpid-broker-0.16)
  * zeno simulator avatar (download and extract to e.g. /home/user_name/zeno_avatar_simulator/)
  * Java 1.6 and 1.7

# Usage:

Step 1: Start ROS master 
    
    roscore

Step 2: Set up/Export path to qpid directory.e.g.
  
    export PATH=/home/user/qpid-broker-0.16/bin:$PATH

Step 3: Start qpid server 

    qpid-server
      Note: requires JAVA_HOME and PATH enviornment variables to be pointing to Java 1.6

### Running with simulation:

Step 4: Set up/Export path to avatar installation directory.e.g.
  
    export Avatar_HOME=/home/user_name/zeno_avatar_simulator/

Step 5: Start zeno avatar simulator
    
    roslaunch zeno_avatar_simulation avatar_simulation.launch
      Note: requires JAVA_HOME and PATH enviornment variables to be pointing to Java 1.6

Step 6: Set up/Export environment variable 'Avatar_Simulation' to true e.g.
  
    export Avatar_Simulation=true

### Running with real robot:

Step 7: Login to zeno using SSH and start required services.

    sudo confctl services start:headless
    sudo confctl services start:qpid

    Note: After running the services, make sure the robot joints are activated.

Step 8: Set up/Export environment variable 'Avatar_Simulation' to false e.g.
 
    export Avatar_Simulation=false


## Launch interface to zeno robokind api

Step 9:

    roslaunch zeno_robokind_interface robokind_interface.launch
      Note: requires JAVA_HOME and PATH enviornment variables to be pointing to Java 1.7

## Test joint motions

Step 10: Launch rqt ui 
    
    roslaunch zeno_rqt_ui rqt_ui.launch

Step 11: Run rqt reconfigure

    rosrun rqt_reconfigure rqt_reconfigure

# Configuring Java environments

  * export JAVA_HOME and PATH environment variable. e.g.

      JAVA_HOME=/usr/lib/jvm/jdk1.6.0_45

      or

      JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64

      export PATH=$JAVA_HOME/bin:$PATH
      export JAVA_HOME
      export PATH

